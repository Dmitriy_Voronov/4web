<?php


header('Content-Type: text/html; charset=UTF-8');

if ($_SERVER['REQUEST_METHOD'] == 'GET') {
 $messages = array();

  if (!empty($_COOKIE['save'])) {
    setcookie('save', '', 100000);
    $messages[] = 'Спасибо, результаты сохранены.';
  }
  $errors = array();
  $errors['fio'] = !empty($_COOKIE['fio_error']);
  $errors['email'] = !empty($_COOKIE['email_error']);
  $errors['data'] = !empty($_COOKIE['data_error']);
  $errors['Limbs'] = !empty($_COOKIE['Limbs_error']);
  $errors['abilities'] = !empty($_COOKIE['abilities_error']);
  $errors['Biografia'] = !empty($_COOKIE['Biografia_error']);
  $errors['Контракт'] = !empty($_COOKIE['Контракт_error']);
  
  if ($errors['fio']) {
    setcookie('fio_error', '', 100000);
    $messages[] = '<div class="error">Заполните имя.</div>';
  }
  if($errors['email']) {
  setcookie('email_error', '', 100000);
  $messages[] = '<div class="error">Заполните почту.</div>';
  }
  if($errors['data']) {
  setcookie('data_error', '', 100000);
  $messages[] = '<div class="error">Заполните дату.</div>';
  }
  if($errors['Limbs']) {
  setcookie('Limbs_error', '', 100000);
  $messages[] = '<div class="error">Выберете количество.</div>';
  }
  if($errors['abilities']) {
  setcookie('abilities_error', '', 100000);
  $messages[] = '<div class="error">Выберете способность.</div>';
  }
  if($errors['Biografia']) {
  setcookie('Biografia_error', '', 100000);
  $messages[] = '<div class="error">Заполните Биографию.</div>';
  }
  if($errors['Контракт']) {
  setcookie('Контракт_error', '', 100000);
  $messages[] = '<div class="error">Нажмите на кнопку.</div>';
  }
  
  $values = array();
  $values['fio'] = empty($_COOKIE['fio_value']) ? '' : $_COOKIE['fio_value'];
  $values['email'] = empty($_COOKIE['email_value']) ? '' : $_COOKIE['email_value'];
  $values['data'] = empty($_COOKIE['data_value']) ? '' : $_COOKIE['data_value'];
  $values['Limbs'] = empty($_COOKIE['Limbs_value']) ? '' : $_COOKIE['Limbs_value'];
  $values['abilities'] = empty($_COOKIE['abilities_value']) ? '' : $_COOKIE['abilities_value'];
  $values['Biografia'] = empty($_COOKIE['Biografia_value']) ? '' : $_COOKIE['Biografia_value'];
  $values['Контракт'] = empty($_COOKIE['Контракт_value']) ? '' : $_COOKIE['Контракт_value'];
  
  include('form.php');
  exit();
  switch($_POST['pol']) {
    case 'm': {
        $pol='m';
        break;
    }
    case 'f':{
        $pol='f';
        break;
    }
};

switch($_POST['Limbs']) {
    case '1': {
        $limbs='1';
        break;
    }
    case '2':{
        $limbs='2';
        break;
    }
    case '3':{
        $limbs='3';
        break;
    }
    case '4':{
        $limbs='4';
        break;
    }
};

$user = 'db';
$pass = '123';
$db = new PDO('mysql:host=localhost;dbname=test', $user, $pass, array(PDO::ATTR_PERSISTENT => true));
try {
  $stmt = $db->prepare("INSERT INTO application (name) SET name = ?");
  $stmt -> execute(array($_POST['fio'],$_POST['email'],$_POST['date'],$pol,$limbs,$abilities,$_POST['Biografia']));
}
catch(PDOException $e){
  print('Error : ' . $e->getMessage());
  exit();
}
header('Location: ?save=1');
}
else {
  $errors = FALSE;
  if (empty($_POST['fio'])) {
    setcookie('fio_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  }
  else {
    setcookie('fio_value', $_POST['fio'], time() + 30 * 24 * 60 * 60);
  }
  if (empty($_POST['email'])) {
    setcookie('email_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  }
  else {
    setcookie('email_value', $_POST['email'], time() + 30 * 24 * 60 * 60);
  }
  if (empty($_POST['data'])) {
    setcookie('data_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  }
  else {
    setcookie('data_value', $_POST['data'], time() + 30 * 24 * 60 * 60);
  }
  if (empty($_POST['Limbs'])) {
    setcookie('Limbs_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  }
  else {
    setcookie('Limbs_value', $_POST['Limbs'], time() + 30 * 24 * 60 * 60);
  }
  if (empty($_POST['abilities'])) {
    setcookie('abilities_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  }
  else {
    setcookie('abilities_value', $_POST['abilities'], time() + 30 * 24 * 60 * 60);
  }
  if (empty($_POST['Biografia'])) {
    setcookie('Biografia_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  }
  else {
    setcookie('Biografia_value', $_POST['Biografia'], time() + 30 * 24 * 60 * 60);
  }
  if (empty($_POST['Контракт'])) {
    setcookie('Контракт_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  }
  else {
    setcookie('Контракт_value', $_POST['Контракт'], time() + 30 * 24 * 60 * 60);
  }
if ($errors) {
    header('Location: index.php');
    exit();
  }
  else {
    setcookie('fio_error', '', 100000);
    setcookie('email_error', '', 100000);
    setcookie('data_error', '', 100000);
    setcookie('Limbs_error', '', 100000);
    setcookie('abilities_error', '', 100000);
    setcookie('Biografia_error', '', 100000);
    setcookie('Контракт_error', '', 100000);
  }
  setcookie('save', '1');
  header('Location: index.php');
}


?>

